FLSUN-QQ TFT32
==============

This repo contains the configuration I'm using for the MKS TFT32 of my FLSUN-QQ printer.

It is based on v3.0.3 of [MKS-TFT](https://github.com/makerbase-mks/MKS-TFT). It uses a simple blue theme.

# Warning

If you use this configuration, CLONE THE REPOSITORY USING GIT!!!. IF YOU DOWNLOAD THE FILES USING DIRECTLY IN A BROWSER, THE BINARY
FILES WILL MOST LIKELY BE DOWNLOADED AS TEXT AND YOU MIGHT BRICK YOUR TFT.

# Use

1. clone the repo.
2. modify config/mks_config.txt file to provide your WIFI configuration. Yes, mine is in there and if you could
find my network you might be able to hack into it, provided of course you can masquerade the MAC address of my WIFI
card.
2. copy the contents of the config folder to the root of your SD Card.
3. put the SD Card in your drive and either start your printer or hook up the USB cable to your computer.

Note, The process of flashing your TFT will modify the contents of the SD Card. It does so to prevent the TFT from
being re-flashed when restarting the printer or TFT with the SD Card still in the SD drive.

# Thoughts

The documentation for most of the stuff out there about open source 3d printers is bad. In addition, even the documentation
for a lot of the proprietary stuff is bad. The MKS TFT 32 is no exception. I was able to find a data sheet for the board,
which I have also put in this repo. It of course does little to help understand how the board works.

The WIFI for the TFT sucks. The hardware might be okay but the software is useless.

# Legal

THESE FILES ARE PROVIDED AS! NO WARRANTIES ARE PROVIDED, INCLUDING ANY IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE,
AND ANY WARRANTIES RELATED TO INTELLECTUAL PROPERTY.

THESE FILES ARE PROVIDED SUBJECT TO THE INTELLECTUAL PROPERTY RIGHTS, IF ANY, OF [MKS-TFT](https://github.com/makerbase-mks/MKS-TFT).

YOU ASSUME ALL RISKS OF USING THE FILES IN THIS REPO.
